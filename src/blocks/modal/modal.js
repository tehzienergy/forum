$('[data-fancybox]').fancybox({
	touch: false
});

/* Тестовый скрипт для демонстрации отправки формы предложения сессии */
$('.modal__offer-form .btn').click(function(e) {
  e.preventDefault();
  $('.modal__offer-form').hide();
  $('.modal__offer-sent').fadeIn();
})
