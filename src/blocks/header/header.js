$(window).on('load scroll', function () {
  var scrolled = $(this).scrollTop();
  var header = $('.header');

  if (scrolled > 1) {
    header.addClass('header--fixed');
  } else {
    header.removeClass('header--fixed');
  }
});
