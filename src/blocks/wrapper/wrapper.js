$(document).on('click', '.js-btn-anchor', function (event) {
  event.preventDefault();

  $('html, body').animate({
    scrollTop: $($.attr(this, 'href')).offset().top
  }, 500);
  $('.menu__btn').removeClass('menu__btn--active');
  $('.menu__content').removeClass('menu__content--active');
  $('body').removeClass('fixed');
});
